survivorlib
==================================================
[![Build Status](https://travis-ci.org/nul-one/survivorlib.png)](https://travis-ci.org/nul-one/survivorlib)
[![PyPI version](https://badge.fury.io/py/survivorlib.svg)](https://badge.fury.io/py/survivorlib)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
[![Requirements Status](https://requires.io/github/nul-one/survivorlib/requirements.svg?branch=master)](https://requires.io/github/nul-one/survivorlib/requirements/?branch=master)

Download all pdf's from [Survivor Library](http://www.survivorlibrary.com) via [archive.org](https://archive.org/details/survival.library).

Installation
-------------------------

### install from pypi (recommend)
`pip3 install survivorlib`

### install from git (latest master)
`pip3 install -U git+https://gitlab.com/nul.one/survivorlib.git`

Usage
-------------------------

To download complete library with default options, just run `survivorlib` in the directory where you want pdf files to be stored.

There are some additional options available to tweak your download:

```
-w, --workers INTEGER  Number of workers that download pdf's.  [default: 4]
-l, --list-sections    List available sections and exit.
-s, --section TEXT     Specify single section to download.
--help                 Show this message and exit.
```

