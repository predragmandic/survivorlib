"""
Download all pdf files from www.survivorlibrary.com
"""

__version__ = "0.2.0"
__licence__ = "BSD"
__year__ = "2018"
__author__ = "Predrag Mandic"
__author_email__ = "info@nul.one"

